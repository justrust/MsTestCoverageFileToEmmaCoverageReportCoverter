﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Text;
using Microsoft.VisualStudio.Coverage.Analysis;

namespace KA.CoverageCoverter
{
    using XmlProcessing;    

    internal class Program
    {
        public static int Main(string[] args)
        {
            try
            {
                var path = args[0];
                if (string.IsNullOrWhiteSpace(path))
                    path = "TestResults";
                foreach (string file in Directory.EnumerateFiles(path, "*.coverage", SearchOption.AllDirectories))
                {
                    var coverageDS = ConvertToCoverageDSPriv(file);
                    var memorySTream = GetMemoryStreamForcoverageDS(coverageDS);
                    SaveCoverageDSFileIfNeeded(args, path, coverageDS);
                    SaveXslTransformedCoverageFile(path, memorySTream);
                    return 0;
                }
            }
            catch (Exception e)
            {

                Console.WriteLine("Error writing to output file: {0}", e.Message);
                return 1;
            }
            return 0;
        }

        private static MemoryStream GetMemoryStreamForcoverageDS(CoverageDS coverageDS)
        {
            var memorySTream = new MemoryStream();            
            var xmlWriter = XmlWriter.Create(memorySTream, new XmlWriterSettings { ConformanceLevel = ConformanceLevel.Auto });
            coverageDS.WriteXml(xmlWriter);
            xmlWriter.Flush();
            xmlWriter.Close();
            return memorySTream;
        }

        private static void SaveXslTransformedCoverageFile(string path, MemoryStream memorySTream)
        {
            var str = Encoding.UTF8.GetString(memorySTream.ToArray());
            memorySTream.Seek(0, SeekOrigin.Begin);
            var xmlReader = XmlReader.Create(memorySTream);
            var outputFileName = Path.Combine(path, "Results.coverage.Xml");
            SaveXsltTransformed(xmlReader, outputFileName);
        }

        private static void SaveCoverageDSFileIfNeeded(string[] args, string path, CoverageDS coverageDS)
        {
            const string coverageDSParam = "coverageDS:";
            if (!args.Any(s => s != null && s.StartsWith(coverageDSParam))) return;
            var coverageDSFileName = args.Where(s => s != null && s.StartsWith(coverageDSParam)).FirstOrDefault().Replace(coverageDSParam, String.Empty).Trim();
            var coverageDSFilePath = Path.Combine(path, coverageDSFileName);
            Console.WriteLine(LocalizedTexts.ConsoleOutputSavingCoverageDSFileTo, coverageDSFilePath);
            coverageDS.WriteXml(coverageDSFilePath);
        }

        private static CoverageDS ConvertToCoverageDSPriv(string file)
        {
            string path = Path.GetDirectoryName(file);
            Console.WriteLine(LocalizedTexts.ConsoleOutputProcessingFile, file);
            var info = CoverageInfo.CreateFromFile(file, new string[] { path }, new string[] { });
            return info.BuildDataSet();
        }

        private static void SaveXsltTransformed(XmlReader sourceXmlReader, string destinationPath)
        {
            var xslXmlReader = XmlReader.Create(new StringReader(Xsls.MSTestCoverageToEmma));
            var command = new DoXmlXsltTransformationCommand();
            Console.WriteLine(LocalizedTexts.ConsoleOutputWritingEmmaReportToFile, destinationPath);
            using (var output = File.Create(destinationPath))
                using (var xmlWriter = XmlWriter.Create(output, new XmlWriterSettings { ConformanceLevel = ConformanceLevel.Auto }))
                    command.Execute(xslXmlReader, sourceXmlReader, xmlWriter);
        }
    }
}