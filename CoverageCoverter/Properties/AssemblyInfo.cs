﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("KA.CoverageCoverter")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("MsTestCoverageFileToEmmaCoverageReportCoverter")]
[assembly: AssemblyCopyright("Copyright © Konstantinos Apostolidis 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("794a511f-d875-4c5c-8d7d-c2c155cc188c")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: InternalsVisibleTo("KA.CoverageCoverter.Tests, PublicKey=002400000480000094000000060200000024000052534131000400000100010045fb2b410e2792c84ccd0fc02222c78a741666706da92eddb445b584d3f436e5fbeb26859a6a11e0a64e7d4a47ea7d574e2511f6521a3bb7dee769e1e4658d4e0550cb6a5838738eface4eacf07665143f613ca7e42f50a5c970b704c59eaad721a53021bdd3528eded754792884eb4cffb85f9719f2aedf61f094ee54c3daec")]