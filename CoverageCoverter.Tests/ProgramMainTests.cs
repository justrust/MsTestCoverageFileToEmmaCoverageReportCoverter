﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace KA.CoverageCoverter
{
    [ExcludeFromCodeCoverage, TestClass]
    [DeploymentItem("TestCoverageData.coverage")]
    public class ProgramMainTests
    {
        [TestMethod]
        public void ProgramMain_ConvertsAndReturnes_0()
        {
            //var sourceFile = "TestCoverageData.coverage";
            var destinationFile = @"..\..\Result.coverage.Xml";
            var result = Program.Main(new[] { ".", destinationFile });
            Assert.AreEqual(result, 0);
        }
    }
}