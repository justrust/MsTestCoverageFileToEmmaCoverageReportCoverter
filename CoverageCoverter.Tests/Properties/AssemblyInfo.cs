using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("KA.CoverageCoverter.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("MsTestCoverageFileToEmmaCoverageReportCoverter")]
[assembly: AssemblyCopyright("Copyright © Konstantinos Apostolidis 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("388b0e35-b2e9-4f94-98cb-851e9fb338da")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]