﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KA.XmlProcessing
{
    [ExcludeFromCodeCoverage, TestClass]
    [DeploymentItem("vstest.coverage.xml")]
    public class DoXmlXsltTransformationCommandTests
    {
        [TestMethod]
        public void DoXmlXsltTransformationCommand()
        {
            var command = new DoXmlXsltTransformationCommand();
            var xslXmlReader = XmlReader.Create(new StringReader(Xsls.MSTestCoverageToEmma));
            var path = Directory.GetCurrentDirectory();
            var outputFileName = "vstest.coverageResult.xml";
            var inputFileBane = "vstest.coverage.xml";
            using (var output = XmlWriter.Create(outputFileName, new XmlWriterSettings { ConformanceLevel = ConformanceLevel.Fragment }))
            using (var input = XmlReader.Create(File.OpenText(inputFileBane)))
                command.Execute(xslXmlReader, input, output);
            Console.Write(File.ReadAllText(outputFileName));
        }
    }
}