using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("KA.XmlProcessing.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("KA.XmlProcessing")]
[assembly: AssemblyCopyright("Copyright © Konstantinos Apostolidis 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("97b574a1-ead3-483c-8d77-bfff3850a870")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]