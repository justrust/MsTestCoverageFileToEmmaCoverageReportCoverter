﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("KA.XmlProcessing.Contracts")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("KA.XmlProcessing")]
[assembly: AssemblyCopyright("Copyright © Konstantinos Apostolidis  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("80ac0d86-f525-447d-97ef-89c75225090c")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]