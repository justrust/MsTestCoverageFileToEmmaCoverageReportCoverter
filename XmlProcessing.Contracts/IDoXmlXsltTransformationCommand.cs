﻿using System.Xml;

namespace KA.XmlProcessing
{
    public interface IDoXmlXsltTransformationCommand
    {
        void Execute(XmlReader xsl, XmlReader sourceReader, XmlWriter resultWriter);
    }
}