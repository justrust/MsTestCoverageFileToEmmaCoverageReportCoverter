﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("KA.XmlProcessing.Core")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("KA.XmlProcessing")]
[assembly: AssemblyCopyright("Copyright © Konstantinos Apostolidis 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("31263345-ec51-4abf-af0a-0ffb257c07c6")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]