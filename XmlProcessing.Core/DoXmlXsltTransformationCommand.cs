﻿using System.Xml;
using System.Xml.Xsl;

namespace KA.XmlProcessing
{
    public class DoXmlXsltTransformationCommand : IDoXmlXsltTransformationCommand
    {
        public void Execute(XmlReader xsl, XmlReader sourceReader, XmlWriter resultWriter)
        {
            var myXslTrans = new XslCompiledTransform();
            myXslTrans.Load(xsl);
            myXslTrans.Transform(sourceReader, resultWriter);
        }
    }
}